+++
name = "Eduardo Aranda Hernández"
nickname = "eduardoarandah"
description = "Full Stack PHP, Laravel, Wordpress, Javascript, Vue.js, CSS, browser extensions"
skills = "Full Stack Web Developer"
thumbnail = "/img/directorio/eduardo-aranda.jpg"
draft = false
date = "2020-02-06"
disponibility = "Disponible para proyectos"
rol = "miembro"
website = "https://eduardoarandah.github.io/"
author = "eduardoarandah"
+++

Me llamo Eduardo Aranda, Ingeniero en T.I. por la Anáhuac

+15 años de experiencia en desarrollo de sistemas.

Soy especialista en tecnologías web, Full-Stack Developer.

Un Full-Stack es capaz de planear y desarrollar todas las capas que componen un sistema web, desde la configuración del servidor en Linux, la programación, seguridad y optimización para alto rendimiento.


## Programación en Laravel

Soy parte del equipo "Laravel Backpack" el cual desarrolla un backend muy usado para Laravel

![Laravel Newsletter](/img/directorio/eduardoarandah/laravel-backpack-team.png)

[https://github.com/orgs/Laravel-Backpack/people](https://github.com/orgs/Laravel-Backpack/people)


## Paquetes Open Source

Soy desarrollador de paquetes de software, los cuales puede ver en mi página de github.

[https://eduardoarandah.github.io/](https://eduardoarandah.github.io/)

Algunos de mis paquetes, como el [descargador masivo de facturas](https://chrome.google.com/webstore/detail/descarga-masiva-facturas/cmcidfkdmfopijnadkbdbhknfkjodmec) son usados diariamente por miles de personas


## Artículos

He escrito artículos enfocados a programadores, los cuales han sido publicados en algunas newsletters

![Laravel Newsletter](/img/directorio/eduardoarandah/newsletter.png)

## Servicios y productos

- Creación de periódicos online de alto rendimiento. (En el estado de Veracruz llevo la infraestructura de muchos de ellos.)

- Creación de sitios web avanzados con wordpress, sage9, ACF, gutenberg

- Desarrollo de sistemas a la medida del cliente (webapps laravel)

Escríbeme: eduardoarandah@gmail.com
